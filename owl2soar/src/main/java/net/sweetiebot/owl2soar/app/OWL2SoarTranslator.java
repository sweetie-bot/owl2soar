/* This file is part of the OWL API.
 * The contents of this file are subject to the LGPL License, Version 3.0.
 * Copyright 2014, The University of Manchester
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.
 *
 * Alternatively, the contents of this file may be used under the terms of the Apache License, Version 2.0 in which case, the provisions of the Apache License Version 2.0 are applicable instead of those above.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. */
package net.sweetiebot.owl2soar.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.apache.commons.cli.*;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import net.sweetiebot.owl2soar.lib.OWLSoarRulesOntologyFormat;
import net.sweetiebot.owl2soar.lib.OWLSoarRulesRenderer;

import org.semanticweb.owlapi.model.OWLOntology;


public final class OWL2SoarTranslator  {

	public static void help(Options options) {
		String header = "\nConvert OWL ontology to Soar rules. Rules are printed on standart output if output file name is not supplied.\n\n";
    	HelpFormatter formatter = new HelpFormatter();
    	formatter.printHelp("owl2soar INPUT_FILE ", header, options, null, true);
	}

    public static void main(String[] args) throws OWLException, InstantiationException, IllegalAccessException, ClassNotFoundException 
    {
        Options options = new Options();
        options.addOption(new Option("r", "root-classes", true, "Root OWL classes of OWL ontology (after prefix removal). Default: \"Object,Predicate,Concept\"."));
        options.addOption(new Option("o", "output", true, "Output file name."));
        options.addOption(new Option("p", "preserve-prefix", false, "Preserve ontology current ontology default prefix in short form. Otherwice prefix is removed."));
        options.addOption(new Option("h", "help", false, "Print this message."));
        
        CommandLineParser parser = new DefaultParser();;
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
            if (cmd.getArgs().length != 1) throw new ParseException("Not enough arguments.");
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            help(options);
            System.exit(1);
        }
        
        if (cmd.hasOption("help")) {
            help(options);
            System.exit(1);
        }
        
        // open input file
        File in_file = new File(cmd.getArgs()[0]);
        if (!in_file.canRead()) {
        	System.out.println(String.format("Unable to open file: %s", cmd.getArgs()[0]));
    		System.exit(1);	
        }
        
        // load ontology
        IRI documentIRI = IRI.create(in_file);
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(documentIRI);   
        
        // create renderer format
        OWLSoarRulesOntologyFormat format = new OWLSoarRulesOntologyFormat();
        if (! cmd.hasOption("preserve-prefix")) {
        	String default_prefix = ontology.getFormat().asPrefixOWLDocumentFormat().getDefaultPrefix();
        	format.setParameter("remove_prefix", default_prefix);
        }
        format.setParameter("root_labels", cmd.getOptionValue("root-classes", "Object, Predicate, Concept"));
        /// and corresponding renderer
        OWLSoarRulesRenderer renderer = new OWLSoarRulesRenderer(format);
        
        // render ontology to file or standard output
        if (cmd.hasOption("output")) {
        	File out_file = new File(cmd.getOptionValue("output"));
        	try {
        		PrintWriter out = new PrintWriter(out_file);
        		renderer.render(ontology, out);
        	}
        	catch (FileNotFoundException e) {
        		System.out.println(String.format("Unable to open file: %s: %s", cmd.getOptionValue("output"), cmd.getArgs()[0]));
        		System.exit(1);	
        	}
        }
        else {
        	// render to standart output.
        	renderer.render(ontology, System.out);
        }       
    }
}

