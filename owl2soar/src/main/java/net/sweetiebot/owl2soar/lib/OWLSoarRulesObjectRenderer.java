/* This file is part of the OWL API.
 * The contents of this file are subject to the LGPL License, Version 3.0.
 * Copyright 2014, The University of Manchester
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.
 *
 * Alternatively, the contents of this file may be used under the terms of the Apache License, Version 2.0 in which case, the provisions of the Apache License Version 2.0 are applicable instead of those above.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. */
package net.sweetiebot.owl2soar.lib;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.Nonnull;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectVisitor;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLPropertyExpression;
import org.semanticweb.owlapi.model.OWLRuntimeException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom;
import org.semanticweb.owlapi.model.SWRLArgument;
import org.semanticweb.owlapi.model.SWRLAtom;
import org.semanticweb.owlapi.model.SWRLClassAtom;
import org.semanticweb.owlapi.model.SWRLObjectPropertyAtom;
import org.semanticweb.owlapi.model.SWRLRule;
import org.semanticweb.owlapi.model.SWRLVariable;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
// import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.util.NamespaceUtil;

// import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.semanticweb.HermiT.ReasonerFactory;


@SuppressWarnings({"javadoc"})
public class OWLSoarRulesObjectRenderer implements OWLObjectVisitor {
	@Nonnull
    private final Writer writer;
    //private final DefaultPrefixManager prefix_manager;
    @Nonnull
    private final OWLReasonerFactory reasonerFactory;
    @Nonnull
    private final Set<String> root_class_labels;
    private final String remove_prefix;
    
    private OWLReasoner reasoner;
    private NamespaceUtil namespaceUtil;
    private StringBuilder buffer;
    private int swrl_rule_number;
    
    public OWLSoarRulesObjectRenderer(Writer writer, OWLSoarRulesOntologyFormat format) {
        this.writer = writer;

        // reasonerFactory = new StructuralReasonerFactory();
        reasonerFactory = new ReasonerFactory();
        
        // process parameters 
        this.root_class_labels = new HashSet<String>();
        String root_labels = format.getParameter("root_labels", "owl::Thing");
        for(String label : root_labels.split(",")) {
        	this.root_class_labels.add(label.trim());
        }
        this.remove_prefix = format.getParameter("remove_prefix", "");
        
        // internal buffer and SWRL rule counter
        buffer = new StringBuilder();
        swrl_rule_number = 0;
    }
    
    public String labelFor(OWLEntity entity) {
    	String namespace = entity.getIRI().getNamespace();
    	if (namespace.equals(remove_prefix)) {	
    		return entity.getIRI().prefixedBy("");
    	}
    	else {
    		String prefix = namespaceUtil.getPrefix(namespace);
    		if (prefix.equals(remove_prefix)) {
    			return entity.getIRI().prefixedBy("");
    		}
    		else {
    			return entity.getIRI().prefixedBy(prefix + ':');
    		}
    	}
    }
    
    public String getShortForm(OWLEntity entity) {
    	String namespace = entity.getIRI().getNamespace();
    	String prefix = namespaceUtil.getPrefix(namespace);
    	return entity.getIRI().prefixedBy(prefix + ':');
    }
    
    public boolean isRootLabel(String s) {
    	return root_class_labels.contains(s);    	
    }
    
    public boolean isRootClass(OWLClass cl) {
    	String label = labelFor(cl);
    	return isRootLabel(label);    	
    }
    
    @SuppressWarnings({"javadoc"})
    public Optional<String> getRootClass(OWLClassExpression cl) {
    	if (cl.isOWLClass()) {
    		String label = labelFor((OWLClass) cl);
    		if (isRootLabel(label)) {
    			Optional<String> opt = Optional.of(label);
    			return opt;
    		}
    	} 
    	NodeSet<OWLClass> superclasses = reasoner.getSuperClasses(cl, false); 
    	for(Node<OWLClass> node : superclasses) {
    		String label = labelFor(node.getRepresentativeElement());        	
    		if (isRootLabel(label)) {
    			Optional<String> opt = Optional.of(label);
    			return opt;
    		}
    	}
    	return Optional.empty();
    }
    
    public Optional<String> getRootClass(OWLObjectPropertyExpression prop) {
    	NodeSet<OWLClass> domains = reasoner.getObjectPropertyDomains(prop);
		
		Optional<String> label = Optional.empty();
		for(Node<OWLClass> node : domains) {
    		label = getRootClass(node.getRepresentativeElement());
			if (label.isPresent()) break;
		}
		
		return label;
    }
    
    private void clear() {
    	buffer = new StringBuilder();   
    }
    
    private OWLSoarRulesObjectRenderer write(String s) {
    	buffer.append(s);
    	return this;
    }
    
    private void commit() {
    	try {
    		writer.write(buffer.toString());
    	} 
    	catch (IOException e) {
    		throw new OWLRuntimeException(e);
    	}
    }
    
    @Override
    public void visit(OWLSubClassOfAxiom axiom) {
    	OWLClassExpression subclass_exp = axiom.getSubClass();
        OWLClassExpression superclass_exp = axiom.getSuperClass();
        
        if (subclass_exp.isOWLClass())
        {
        	String subclass = labelFor((OWLClass) subclass_exp);
        	Optional<String> label = getRootClass(subclass_exp);
        		
        	boolean succeed = false;
        	
        	clear();
       	    write(String.format("sp {elaborate*SubClassOf*%s\n", subclass));
       	    write("\t(state <s> ^beliefs <b>)\n");
       	    write(String.format("\t(<b> ^%s <e>)\n", label.orElse("<any>").toLowerCase()));
       	    write(String.format("\t(<e> ^type %s)\n", subclass));
       	    write("-->\n");
       	    for(OWLClassExpression exp : superclass_exp.asConjunctSet()) {
       	    	if (exp.isOWLClass()) {
       	    		String superclass = labelFor((OWLClass) exp);
       	    		if (!isRootLabel(superclass)) {
       	    			write(String.format("\t(<e> ^type %s)\n", superclass));
       	    			succeed = true;
       	    		}
       	    	}
       	    }
       	    write("}\n\n");
       	    if (succeed) commit();
        }
    }
    
    @Override
    public void visit(OWLSubObjectPropertyOfAxiom axiom) {
    	OWLObjectPropertyExpression sub_exp = axiom.getSubProperty();
        OWLObjectPropertyExpression super_exp = axiom.getSuperProperty();
        
        if (sub_exp.isOWLObjectProperty() & super_exp.isOWLObjectProperty()) {
        	String sub_prop = labelFor((OWLObjectProperty) sub_exp);
        	String super_prop = labelFor((OWLObjectProperty) super_exp);
        	Optional<String> label = getRootClass(sub_exp);
        	
        	clear();
        	write(String.format("sp {elaborate*SubObjectPropertyOf*%s\n", sub_prop));
        	write("\t(state <s> ^beliefs <b>)\n");
        	write(String.format("\t(<b> ^%s <e1>)\n", label.orElse("<any>").toLowerCase()));
        	write(String.format("\t(<e1> ^%s <e2>)\n", sub_prop));
        	write("-->\n");
        	write(String.format("\t(<e> ^type %s)\n", super_prop));
        	write("}\n\n");
        	commit();
        }
    }
    
    public void visit(OWLDisjointClassesAxiom axiom) {
    	Set<OWLClassExpression> exps = axiom.getClassExpressions();
    	
    	while (exps.size() >= 2) {
    		OWLClassExpression exp1 = exps.iterator().next();
    		exps.remove(exp1);
    		for(OWLClassExpression exp2 : exps) {
    			if (exp1.isOWLClass() & exp2.isOWLClass()) {
    				String cls1 = labelFor((OWLClass) exp1);
    				String cls2 = labelFor((OWLClass) exp2);
    				
    				if (isRootLabel(cls1) | isRootLabel(cls2)) continue;
    				
    				Optional<String> pool = getRootClass((OWLClass) exp1);
    				if (pool.isEmpty()) pool = getRootClass((OWLClass) exp2);
    				
    				clear();
    	        	write(String.format("sp {elaborate*DisjointClasses*%s*%s\n", cls1, cls2));
    	        	write("\t(state <s> ^beliefs <b>)\n");
    	        	write(String.format("\t(<b> ^%s <e>)\n", pool.orElse("<any>").toLowerCase()));
    	        	write(String.format("\t(<e> ^type %s ^type %s)\n", cls1, cls2));
    	        	write("-->\n");
    	        	write(String.format("\t(<b> ^contradiction DisjointClassesAxiom)\n"));
    	        	write(String.format("\t(<b> ^contradiction-classes %s %s)\n", cls1, cls2));
    	        	write(String.format("\t(<b> ^contradiction-entity <e>)\n"));
    	        	write("}\n\n");
    	        	commit();
    			}
    		}	
    	}
    }
    
    public void visit(OWLDisjointObjectPropertiesAxiom axiom) {
    	Set<OWLObjectPropertyExpression> exps = axiom.getProperties();
    	
    	while (exps.size() >= 2) {
    		OWLObjectPropertyExpression exp1 = exps.iterator().next();
    		exps.remove(exp1);
    		for(OWLObjectPropertyExpression exp2 : exps) {
    			if (exp1.isOWLObjectProperty() & exp2.isOWLObjectProperty()) {
    				String prop1 = labelFor((OWLObjectProperty) exp1);
    				String prop2 = labelFor((OWLObjectProperty) exp2);
       		
    				Optional<String> pool = getRootClass(exp1);
    				if (pool.isPresent() & !pool.equals(getRootClass(exp2))) break;
    				
    				clear();
    	        	write(String.format("sp {elaborate*DisjointObjectProperties*%s*%s\n", prop1, prop2));
    	        	write("\t(state <s> ^beliefs <b>)\n");
    	        	write(String.format("\t(<b> ^%s <e1>)\n", pool.orElse("<any>").toLowerCase()));
    	        	write(String.format("\t(<e1> ^%s <e2>)\n", prop1));
    	        	write(String.format("\t(<e1> ^%s <e2>)\n", prop2));
    	        	write("-->\n");
    	        	write(String.format("\t(<b> ^contradiction DisjointObjectPropertiesAxiom)\n"));
    	        	write(String.format("\t(<b> ^contradiction-properties %s %s)\n", prop1, prop2));
    	        	write(String.format("\t(<b> ^contradiction-entity <e1>)\n"));
    	        	write("}\n\n");
    	        	commit();
    			}
    		}	
    	}
    }
    
    @Override
    public void visit(OWLSubPropertyChainOfAxiom axiom) {
    	List<OWLObjectPropertyExpression> chain_exp = axiom.getPropertyChain();
    	
    	OWLObjectPropertyExpression super_exp = axiom.getSuperProperty();
    	if (!super_exp.isOWLObjectProperty()) return;
    	String super_prop = labelFor((OWLObjectProperty) super_exp);
    	
    	Optional<String> label = getRootClass(chain_exp.get(0));
    	
    	clear();
    	write(String.format("sp {elaborate*SubPropertyChainOf*%s\n", super_prop));
		write("\t(state <s> ^beliefs <b>)\n");
		write(String.format("\t(<b> ^%s <e1>)\n", label.orElse("<any>").toLowerCase()));
		
		int current = 1;
		for(OWLObjectPropertyExpression exp : chain_exp) {
			if (!exp.isOWLObjectProperty()) return;
			String prop = labelFor((OWLObjectProperty) exp);
			
			write(String.format("\t(<e%d> ^%s <e%d>)\n", current, prop, current+1));
			
			current++;
		}
		write("-->\n");
        write(String.format("\t(<e1> ^%s <e%d>)\n", super_prop));
        write("}\n\n");
        commit();
    }
    
    @Override
    public void visit(OWLEquivalentClassesAxiom axiom) { 
    	Set<OWLClassExpression> exps = axiom.getClassExpressions();
        if (exps.size() != 2) return;
        Iterator<OWLClassExpression> it = exps.iterator();
        OWLClassExpression defined_class_exp = it.next();
        OWLClassExpression exp = it.next();
        if (!defined_class_exp.isOWLClass() & exp.isOWLClass()) {
        	OWLClassExpression tmp = defined_class_exp;
        	defined_class_exp = exp;
        	exp = tmp;
        }
        
        String defined_class = labelFor((OWLClass) defined_class_exp);
        Optional<String> pool = getRootClass(defined_class_exp);
        
        int rule_number = 1;
        for(OWLClassExpression exp_conj : exp.asDisjunctSet()) {
        	Optional<String> conj_pool;
        	if (pool.isPresent()) conj_pool = pool;
        	else conj_pool = getRootClass(exp_conj);
        	
        	clear();
        	write(String.format("sp {elaborate*EqvivalentClasses*%s*%d\n", defined_class, rule_number));
	       	write("\t(state <s> ^beliefs <b>)\n");
	       	write(String.format("\t(<b> ^%s <e>)\n", conj_pool.orElse("<any>").toLowerCase()));
	       	boolean failed = false;
	       	for (OWLClassExpression exp_term : exp_conj.asConjunctSet()) {
	       		if (exp_term.isOWLClass()) {
	       			write(String.format("\t(<e> ^type %s)\n", labelFor((OWLClass) exp_term)));
	       		}
	       		else if (exp_term.getClassExpressionType() == ClassExpressionType.OBJECT_COMPLEMENT_OF) {
	       			OWLObjectComplementOf comp = (OWLObjectComplementOf) exp_term;
	       			OWLClassExpression cls = comp.getOperand();
	       			
	       			if (cls.isOWLClass()) {
	       				write(String.format("\t(<e> -^type %s)\n", labelFor((OWLClass) cls)));
	       			}
	       			else failed = true;
	       		}
	       		else if (exp_term.getClassExpressionType() == ClassExpressionType.OBJECT_SOME_VALUES_FROM) {
	       			OWLObjectSomeValuesFrom role = (OWLObjectSomeValuesFrom) exp_term;
	       			OWLPropertyExpression prop = role.getProperty();
	       			OWLClassExpression cls = role.getFiller();
	       			
	       			if (prop.isOWLObjectProperty()  & cls.isOWLClass()) {
	       				write(String.format("\t(<e> ^%s.type %s)\n", labelFor((OWLObjectProperty) prop), labelFor((OWLClass) cls)));
	       			} 
	       			else failed = true;
	       		} else if (exp_term.getClassExpressionType() == ClassExpressionType.OBJECT_ALL_VALUES_FROM) {
	       			OWLObjectAllValuesFrom role = (OWLObjectAllValuesFrom) exp_term;
	       			OWLPropertyExpression prop = role.getProperty();
	       			OWLClassExpression cls = role.getFiller();
	       			
	       			if (prop.isOWLObjectProperty()  & cls.isOWLClass()) {
	       				write(String.format("\t-{(<e> ^%s <tmp>)\n\t\t(<tmp> -^type %s)}\n", labelFor((OWLObjectProperty) prop), labelFor((OWLClass) cls)));
	       			} 
	       			else failed = true;
	       		}
	       		else if (exp_term.getClassExpressionType() == ClassExpressionType.DATA_HAS_VALUE) {
	       			OWLDataHasValue role = (OWLDataHasValue) exp_term;
	       			OWLPropertyExpression prop = role.getProperty();
	       			OWLLiteral val = role.getFiller();
	       			
	       			if (prop.isOWLDataProperty()) {
	       				write(String.format("\t(<e> ^%s %s)\n", labelFor((OWLDataProperty) prop), val.getLiteral()));
	       			} 
	       			else failed = true;
	       		} 
	       		else failed = true;
	       	}
	       	if (!failed) {
	       		write("-->\n");
	       		write(String.format("\t(<e> ^type %s)\n", defined_class));
	       		write("}\n\n");
	       		commit();
	       		rule_number++;
	       	}
        }   
    }
    
    @Override
    public void visit(OWLInverseObjectPropertiesAxiom axiom) { 
    	OWLObjectPropertyExpression src_exp = axiom.getSecondProperty();
    	OWLObjectPropertyExpression dst_exp = axiom.getFirstProperty();
    	
    	if (src_exp.isOWLObjectProperty() & dst_exp.isOWLObjectProperty()) {
    		String src_prop = labelFor((OWLObjectProperty) src_exp);
    		String dst_prop = labelFor((OWLObjectProperty) dst_exp);	
    		Optional<String> src_label = getRootClass(src_exp);
    		Optional<String> dst_label = getRootClass(dst_exp);
    		   		
    		clear();
    		write(String.format("sp {elaborate*InverseOf*%s*%s\n", dst_prop, src_prop));
    		write("\t(state <s> ^beliefs <b>)\n");
    		write(String.format("\t(<b> ^%s <e1>)\n", src_label.orElse("<any>").toLowerCase()));
    		write(String.format("\t(<e1> ^%s <e2>)\n", src_prop));
    		write("-->\n");
    		write(String.format("\t(<e2> ^%s <e1>)\n", dst_prop));
    		write("}\n\n");
    			
    		write(String.format("sp {elaborate*InverseOf*%s*%s\n", src_prop, dst_prop));
    		write("\t(state <s> ^beliefs <b>)\n");
    		write(String.format("\t(<b> ^%s <e1>)\n", dst_label.orElse("<any>").toLowerCase()));
    		write(String.format("\t(<e1> ^%s <e2>)\n", dst_prop));
    		write("-->\n");
    		write(String.format("\t(<e2> ^%s <e1>)\n", src_prop));
    		write("}\n\n");
	       	commit();
    	}     	
    }
    
    class ObjetyPropertySegment {
    	public String prop;
    	public String symbol;
    	
    	public ObjetyPropertySegment(String prop, String symbol) {
    		this.prop = prop;
    		this.symbol = symbol;
    	}
    };
    
    class SoarRuleSegment  
    {
        public String symbol; 
        public Optional<String> pool; 
        public Set<String> classes;
        public Set<ObjetyPropertySegment> obj_props;
        
        public SoarRuleSegment(String symbol) {
        	this.symbol = symbol;
        	pool = Optional.empty();
        	classes = new HashSet<String>();
        	obj_props = new HashSet<ObjetyPropertySegment>();
        }
    };
    
    private boolean fillSWRLRuleVarsMap(Set<SWRLAtom> atoms, Map<SWRLVariable, SoarRuleSegment> vars_map) {
    	for (SWRLAtom atom : atoms) {	
    		if (atom instanceof SWRLClassAtom) {
    			SWRLClassAtom class_atom = (SWRLClassAtom) atom;
    			
    			SoarRuleSegment linked_class = vars_map.get(class_atom.getArgument());
    			if (linked_class == null) return false;
    			
    			OWLClassExpression exp = class_atom.getPredicate();
    			if (!exp.isOWLClass()) return false;
    			
    			linked_class.classes.add(labelFor((OWLClass) exp));
    			if (linked_class.pool.isEmpty()) {
    				linked_class.pool = getRootClass((OWLClass) exp);
    			}
    		}
    		else if (atom instanceof SWRLObjectPropertyAtom) {
    			SWRLObjectPropertyAtom prop_atom = (SWRLObjectPropertyAtom) atom;
    			
    			SoarRuleSegment linked_class = vars_map.get(prop_atom.getFirstArgument());
    			if (linked_class == null) return false;
    			
    			OWLObjectPropertyExpression exp = prop_atom.getPredicate();
    			if (!exp.isOWLObjectProperty()) return false;
    			String prop = labelFor((OWLObjectProperty) exp);
    			
    			SWRLArgument arg2 = prop_atom.getSecondArgument();
    			if (! (arg2 instanceof SWRLVariable) ) return false;
    			SoarRuleSegment linked_class2 = vars_map.get(arg2);
    			if (linked_class2 == null) return false;
    			String symbol2 = linked_class2.symbol;
    			
    			linked_class.obj_props.add(new ObjetyPropertySegment(prop, symbol2));
    		}
    		else return false;
    	}
    	return true;
    }
    
    @Override
    public void visit(SWRLRule axiom) { 
    	Set<SWRLAtom> body = axiom.getBody();
    	Set<SWRLAtom> head = axiom.getHead();
    	Set<SWRLVariable> vars = axiom.getVariables();
    	Set<OWLAnnotation> annotations = axiom.getAnnotations();
    	
    	int var_number = 0;
    	Map<SWRLVariable, SoarRuleSegment> vars_map = new HashMap<SWRLVariable, SoarRuleSegment>();
    	for(SWRLVariable var : vars) {
    		vars_map.put(var, new SoarRuleSegment(String.format("<v%d>", var_number)));
    		var_number++;
    	}
    	
    	boolean success = fillSWRLRuleVarsMap(body, vars_map);
    	if (!success) return;
    	
    	var_number = 1;
    	for(SoarRuleSegment entity: vars_map.values()) {
    		if (entity.pool.isEmpty()) {
    			entity.pool = Optional.of(String.format("<any%d>", var_number++));			
    		}
    	}
    	
    	String rule_label = "unknown";
    	for(OWLAnnotation annotation : annotations) {
    		String prop = labelFor(annotation.getProperty());
    		if ( prop.equals("rdfs:label") ) {
    			rule_label = ((OWLLiteral) annotation.getValue()).getLiteral();
    			break;
    		}	
    	}
    		
    	clear();
     	write(String.format("sp {elaborate*SWRLRule*%d*%s\n", swrl_rule_number+1, rule_label));
    	write("\t(state <s> ^beliefs <b>)\n");
    	for(SoarRuleSegment entity: vars_map.values()) {
    		if (entity.classes.size() != 0 | entity.obj_props.size() != 0) {
    			write(String.format("\t(<b> ^%s %s)\n", entity.pool.get().toLowerCase(), entity.symbol));
    			for(String cls : entity.classes) {
    				write(String.format("\t(%s ^type %s)\n", entity.symbol, cls));
    			}
    			for(ObjetyPropertySegment prop : entity.obj_props) {
    				write(String.format("\t(%s ^%s %s)\n", entity.symbol, prop.prop, prop.symbol));
    			}
    		}
    	}
    	write("-->\n");		
    	
    	for(SoarRuleSegment entity: vars_map.values()) {
    		entity.classes.clear();
    		entity.obj_props.clear();
    	}
    	
    	success = fillSWRLRuleVarsMap(head, vars_map);
    	if (!success) return;
    	
    	for(SoarRuleSegment entity: vars_map.values()) {
    		for(String cls : entity.classes) {
    			write(String.format("\t(%s ^type %s)\n", entity.symbol, cls));
    		}
    		for(ObjetyPropertySegment prop : entity.obj_props) {
    			write(String.format("\t(%s ^%s %s)\n", entity.symbol, prop.prop, prop.symbol));
    		}
    	}
    	write("}\n\n");
    	commit();
    	swrl_rule_number++;
    }
    
    @Override
    public void visit(OWLOntology ontology) { 
    	// we need reasoner to derive root classes and properties domains
    	reasoner = reasonerFactory.createReasoner(ontology);
    	
    	// use NamespaceUtil instead of PrefixManager from ontology: it 
    	// can silently generate prefixes if they are not provided
    	namespaceUtil = new NamespaceUtil();
    	OWLDocumentFormat fmt = ontology.getFormat();
    	if (fmt.isPrefixOWLDocumentFormat()) {
    		Map<String, String> prefix_map = fmt.asPrefixOWLDocumentFormat().getPrefixName2PrefixMap();
    		for( Map.Entry<String, String> entry : prefix_map.entrySet()) {
    			namespaceUtil.setPrefix(entry.getKey(), entry.getValue());
    		}
    	}
    	
        // Report information about the ontology
    	clear();
        write("# Autogenerated by OWL2Soar\n");
        write(String.format("# Source ontology: %s\n\n", ontology.getOntologyID().toString()));
        commit();
    	
        // visit ontology axioms
        ontology.axioms().forEach(ax -> {
            ax.accept(this);
        });
        
        // free memory
        namespaceUtil = null;
        reasoner  = null;
    }
    
}
